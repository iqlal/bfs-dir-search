import os
import time
from collections import deque

import PyQt5.QtCore

from PyQt5.QtWidgets import QApplication, QWidget, QVBoxLayout, QHBoxLayout, QLabel, QLineEdit, QPushButton, \
    QFileDialog, QTableWidget, QTableWidgetItem, QHeaderView, QMenu, QAction, QInputDialog, QMessageBox


class FileSearchApp(QWidget):
    def __init__(self):
        super().__init__()

        self.init_ui()

    def init_ui(self):
        self.setWindowTitle('File Search App')
        self.setGeometry(100, 100, 800, 600)

        self.start_directory_label = QLabel('Root Directory')
        self.start_directory_edit = QLineEdit()
        self.browse_button = QPushButton('Browse')
        self.browse_button.clicked.connect(self.browse_directory)

        self.target_name_label = QLabel('Filename')
        self.target_name_edit = QLineEdit()

        self.search_button = QPushButton('Search')
        self.search_button.clicked.connect(self.start_search)

        self.result_label = QLabel('Result')
        self.result_table = QTableWidget(self)
        self.result_table.setColumnCount(4)
        self.result_table.setHorizontalHeaderLabels(['File Path', 'Name', 'Created Date', 'Modified Date'])
        self.result_table.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.result_table.horizontalHeader().setSectionsMovable(True)
        self.result_table.horizontalHeader().setSectionsClickable(True)

        self.result_table.setContextMenuPolicy(PyQt5.QtCore.Qt.CustomContextMenu)
        self.result_table.customContextMenuRequested.connect(self.show_context_menu)

        # Layout
        layout = QVBoxLayout()

        directory_layout = QHBoxLayout()
        directory_layout.addWidget(self.start_directory_label)
        directory_layout.addWidget(self.start_directory_edit)
        directory_layout.addWidget(self.browse_button)

        layout.addLayout(directory_layout)
        layout.addWidget(self.target_name_label)
        layout.addWidget(self.target_name_edit)
        layout.addWidget(self.search_button)
        layout.addWidget(self.result_label)
        layout.addWidget(self.result_table)

        self.setLayout(layout)

        # Context Menu Actions
        self.rename_action = QAction('Rename', self)
        self.rename_action.triggered.connect(self.rename_file)

        self.delete_action = QAction('Delete', self)
        self.delete_action.triggered.connect(self.delete_file)

    def browse_directory(self):
        directory = QFileDialog.getExistingDirectory(self, 'Select Root Directory')
        if directory:
            self.start_directory_edit.setText(directory)

    def start_search(self):
        start_directory = self.start_directory_edit.text()
        target_name = self.target_name_edit.text()

        if not start_directory or not target_name:
            return

        self.result_table.setRowCount(0)

        queue = deque([start_directory])

        while queue:
            current_directory = queue.popleft()

            try:
                for filename in os.listdir(current_directory):
                    file_path = os.path.join(current_directory, filename)

                    if os.path.isfile(file_path) and target_name.lower() in filename.lower():
                        created_time = time.ctime(os.path.getctime(file_path))
                        modified_time = time.ctime(os.path.getmtime(file_path))

                        row_position = self.result_table.rowCount()
                        self.result_table.insertRow(row_position)

                        self.result_table.setItem(row_position, 0, QTableWidgetItem(file_path))
                        self.result_table.setItem(row_position, 1, QTableWidgetItem(filename))
                        self.result_table.setItem(row_position, 2, QTableWidgetItem(created_time))
                        self.result_table.setItem(row_position, 3, QTableWidgetItem(modified_time))

                    elif os.path.isdir(file_path):
                        queue.append(file_path)

            except PermissionError as e:
                self.show_permission_error_popup(e)

    def show_permission_error_popup(self, error):
        error_message = f"PermissionError: {error}"
        QMessageBox.critical(self, 'Permission Error', error_message, QMessageBox.Ok)

    def show_context_menu(self, position):
        context_menu = QMenu(self)
        context_menu.addAction(self.rename_action)
        context_menu.addAction(self.delete_action)
        context_menu.exec_(self.result_table.mapToGlobal(position))

    def rename_file(self):
        selected_row = self.result_table.currentRow()
        if selected_row >= 0:
            old_file_path = self.result_table.item(selected_row, 0).text()
            new_name, ok = QInputDialog.getText(self, 'Rename File', 'Enter new name:')
            if ok and new_name:
                new_file_path = os.path.join(os.path.dirname(old_file_path), new_name)
                os.rename(old_file_path, new_file_path)
                self.start_search()  # Refresh the search results

    def delete_file(self):
        selected_row = self.result_table.currentRow()
        if selected_row >= 0:
            file_path = self.result_table.item(selected_row, 0).text()
            response = QMessageBox.question(self, 'Delete File', f'Are you sure you want to delete {file_path}?',
                                            QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
            if response == QMessageBox.Yes:
                os.remove(file_path)
                self.result_table.removeRow(selected_row)


if __name__ == '__main__':
    app = QApplication([])
    window = FileSearchApp()
    window.show()
    app.exec_()